const products = [
    {
        "ID": 44,
        "SKU": "woo-vneck-tee",
        "Name": "V-Neck T-Shirt",
        "Price": 150,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vneck-tee-2.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-green-1.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-blue-1.jpg"
    },
    {
        "ID": 45,
        "SKU": "woo-hoodie",
        "Name": "Hoodie",
        "Price": 320,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-2.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-blue-1.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-green-1.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-logo-2.jpg"
    },
    {
        "ID": 46,
        "SKU": "woo-hoodie-with-logo",
        "Name": "Hoodie with Logo",
        "Price": 450,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-logo-2.jpg"
    },
    {
        "ID": 47,
        "SKU": "woo-tshirt",
        "Name": "T-Shirt",
        "Price": 180,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/tshirt-2.jpg"
    },
    {
        "ID": 48,
        "SKU": "woo-beanie",
        "Name": "Beanie",
        "Price": 200,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-2.jpg"
    },
    {
        "ID": 58,
        "SKU": "woo-belt",
        "Name": "Belt",
        "Price": 650,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/belt-2.jpg"
    },
    {
        "ID": 60,
        "SKU": "woo-cap",
        "Name": "Cap",
        "Price": 180,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/cap-2.jpg"
    },
    {
        "ID": 62,
        "SKU": "woo-sunglasses",
        "Name": "Sunglasses",
        "Price": 900,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/sunglasses-2.jpg"
    },
    {
        "ID": 64,
        "SKU": "woo-hoodie-with-pocket",
        "Name": "Hoodie with Pocket",
        "Price": 450,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-pocket-2.jpg"
    },
    {
        "ID": 66,
        "SKU": "woo-hoodie-with-zipper",
        "Name": "Hoodie with Zipper",
        "Price": 450,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-zipper-2.jpg"
    },
    {
        "ID": 68,
        "SKU": "woo-long-sleeve-tee",
        "Name": "Long Sleeve Tee",
        "Price": 250,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/long-sleeve-tee-2.jpg"
    },
    {
        "ID": 70,
        "SKU": "woo-polo",
        "Name": "Polo",
        "Price": 200,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/polo-2.jpg"
    },
    {
        "ID": 73,
        "SKU": "woo-album",
        "Name": "Album",
        "Price": 150,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/album-1.jpg"
    },
    {
        "ID": 75,
        "SKU": "woo-single",
        "Name": "Single",
        "Price": 30,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/single-1.jpg"
    },
    {
        "ID": 76,
        "SKU": "woo-vneck-tee-red",
        "Name": "V-Neck T-Shirt - Red",
        "Price": 200,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vneck-tee-2.jpg"
    },
    {
        "ID": 77,
        "SKU": "woo-vneck-tee-green",
        "Name": "V-Neck T-Shirt - Green",
        "Price": 200,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-green-1.jpg"
    },
    {
        "ID": 78,
        "SKU": "woo-vneck-tee-blue",
        "Name": "V-Neck T-Shirt - Blue",
        "Price": 150,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-blue-1.jpg"
    },
    {
        "ID": 79,
        "SKU": "woo-hoodie-red",
        "Name": "Hoodie - Red, No",
        "Price": 45,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-2.jpg"
    },
    {
        "ID": 80,
        "SKU": "woo-hoodie-green",
        "Name": "Hoodie - Green, No",
        "Price": 450,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-green-1.jpg"
    },
    {
        "ID": 81,
        "SKU": "woo-hoodie-blue",
        "Name": "Hoodie - Blue, No",
        "Price": 450,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-blue-1.jpg"
    },
    {
        "ID": 83,
        "SKU": "Woo-tshirt-logo",
        "Name": "T-Shirt with Logo",
        "Price": 180,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/t-shirt-with-logo-1.jpg"
    },
    {
        "ID": 85,
        "SKU": "Woo-beanie-logo",
        "Name": "Beanie with Logo",
        "Price": 200,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-with-logo-1.jpg"
    },
    {
        "ID": 87,
        "SKU": "logo-collection",
        "Name": "Logo Collection",
        "Price": 700,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/logo-1.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-with-logo-1.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/t-shirt-with-logo-1.jpg, https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-logo-2.jpg"
    },
    {
        "ID": 89,
        "SKU": "wp-pennant",
        "Name": "WordPress Pennant",
        "Price": 110.5,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/pennant-1.jpg"
    },
    {
        "ID": 90,
        "SKU": "woo-hoodie-blue-logo",
        "Name": "Hoodie - Blue, Yes",
        "Price": 450,
        "Images": "https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-logo-2.jpg"
    }
]