let cartList = []

$(document).ready(function() {
    init();
    attachEvents();
})

function init() {
    const $productList = $('#productList')
    products.forEach(item => {
        let product = $(`
            <li id="${item.ID}" class="product">
                <div class="name">${item.Name}</div>
                <div class="image">
                    <img src="${item.Images.split(',')[0]}" alt="${item.Name}">
                </div>
                <div class="price">${item.Price} $</div>
                <div class="actions">
                    <button class="qty-down">-</button>
                    <input class="qty" value="1">
                    <button class="qty-up">+</button>
                    <button class="add">add</button>
                </div>
            </li>
        `)
        $productList.append(product)
    })
}

function attachEvents() {
    const $menuBtn = $('#menu');
    const $productList = $('#productList')
    const $cartListContainer = $('#listContainer')
    $menuBtn.on('click',function() {
        $('#sidebar').toggleClass('open')
    })
    $productList.on('click','.qty-up',function() {
        const $input = $(this).parents('li').find('input')
        const newValue = parseInt( $input.val() ) + 1
        $input.val(newValue || 1)
    })
    $productList.on('click','.qty-down',function() {
        const $input = $(this).parents('li').find('input')
        const newValue = parseInt( $input.val() ) - 1
        $input.val((newValue < 1 ? 1 : newValue) || 1)
    })
    $productList.on('click','.add',function() {
        const $input = $(this).parents('li').find('input')
        const id = $(this).parents('li').attr('id')
        const qty = parseInt( $input.val() )
        addToCart(id,qty)
    })
    $cartListContainer.on('click','.remove',function() {
        const id = $(this).parents('li').attr('id')
        removeFromCart(id)
    })
}

function removeFromCart(id) {
    cartList = cartList.filter( item => item.id !== id)
    renderCart()
}

function addToCart(id,qty) {
    const cartProduct = cartList.findIndex( item => item.id === id)
    if(cartProduct === -1) {
        cartList.push({id:id,qty:qty})
    } else {
        cartList[cartProduct].qty += qty
    }
    renderCart()
}

function renderCart() {
    const $list = $('<ul class="cart-list"></ul>')
    let totalPrice = 0
    cartList.forEach(function(item) {
        const product = products.find(prod => prod.ID.toString() === item.id.toString())
        let $item = $(`
            <li class="cart-item" id="${item.id}">
                <img src="${product.Images.split(',')[0]}" alt="${product.Name}">
                <div class="details">${item.qty} ${product.Name} for ${item.qty * product.Price}$</div>
                <button class="remove">X</button>
            </li>
        `)
        $list.append($item)
        totalPrice += item.qty * product.Price
    })
    $('#listContainer').html($list)
    $('#sidebar').attr('data-items-count',cartList.length)
    $('#total').text(totalPrice+'$')
}